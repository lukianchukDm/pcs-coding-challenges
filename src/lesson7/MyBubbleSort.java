package lesson7;

import java.util.Arrays;

public class MyBubbleSort {
    public static void main(String[] args) {
        int[] arr = {15, 3, 9, 8, 5, 2, 7, 1, 6};
        bubblesort(arr);
        System.out.println(Arrays.toString(arr));

    }

    private static void bubblesort(int[] arr) {
        boolean isSorted = false;

        while (!isSorted) {
            isSorted = true;
            // before looping assume it is sorted

            int sorted = arr.length - 1;

            for (int i = 0; i < sorted; i++) {
                // if not sorted --> swap
                // else break out the loop with isSorted = true
                if (arr[i] > arr[i + 1]) {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;

                    isSorted = false;
                    //don't break while
                }
            }

            sorted--;
        }
    }


}
