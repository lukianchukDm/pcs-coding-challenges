package lesson7;

import java.util.Arrays;

public class MyQuickSort {
    public static void main(String[] args) {
        int[] arr = {15, 3, 9, 8, 5, 2, 7, 1, 6};
        quicksort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    private static void quicksort(int[] arr, int left, int right) {
        if (left >= right) return;

        int mid = left + (right - left) / 2;
        int pivot = arr[mid];

        int partitionPoint = partition(arr, left, right, pivot);

        quicksort(arr, left, partitionPoint - 1);
        quicksort(arr, partitionPoint, right);
    }

    private static int partition(int[] arr, int left, int right, int pivot) {
        //move pointers towards each other
        while (left <= right) {
            while (arr[left] < pivot) left++;
            // this will break when we get el on left greater than pivot
            while (arr[right] > pivot) right--;

            if (left <= right) {
                int tmp = arr[left];
                arr[left] = arr[right];
                arr[right] = tmp;

                left++;
                right--;
            }
        }

        return left;
    }
}
