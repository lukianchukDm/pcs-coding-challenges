package lesson1;

public class HW_ReverseString_Array {

    public static void main(String[] args) {
        char[] arr1 = {'H', 'e', 'l', 'l', 'o'};
        char[] arr2 = {'C', 'h', 'a', 'l', 'l', 'e', 'n', 'g', 'e', 's'};

        char[] reversedArr1 = reverseString(arr1);
        char[] reversedArr2 = reverseString(arr2);

        System.out.println(reversedArr1);
        System.out.println(reversedArr2);
    }

    private static char[] reverseString(char[] arr) {
        int left = 0;
        int right = arr.length - 1;

        while (left < right) {
            char temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;

            left++;
            right--;
        }

        return arr;
    }

}




/* HINTS
1. You can not traverse the array of chars the same way we traversed the array of integers
2. Since you are not allowed to initialize new array, you need to switch elements in existing array
3. In order to switch the values of 2 elements and not to lose of one them, save this value to a temporary variable
 */
