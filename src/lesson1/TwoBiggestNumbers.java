package lesson1;

public class TwoBiggestNumbers {

    public static void main(String[] args) {
        int[] arr = {115, 24, 5, -33, 17, 7};
        int[] twoHighestNumbers = findTwoHighestNumbers(arr);

        System.out.println("The first highest number is " + twoHighestNumbers[0]);
        System.out.println("The second highest number is " + twoHighestNumbers[1]);
    }

    private static int[] findTwoHighestNumbers(int[] arr) { //O(n) time
        int firstHighest = Integer.MIN_VALUE;
        int secondHighest = Integer.MIN_VALUE;

        for (int element : arr) {
            if (element > firstHighest) {
                secondHighest = firstHighest;
                firstHighest = element;
            } else if (element > secondHighest)
                secondHighest = element;
        }

        return new int[]{firstHighest, secondHighest};
    }
}
