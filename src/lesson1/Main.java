package lesson1;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] arr = {115, 24, 5, -33, 17, 7};

        int[] minMax = findMinMax(arr);
        int[] minMaxSort = findMinMaxSort(arr);

        System.out.println("The min value is " + minMax[0]);
        System.out.println("The max value is " + minMax[1]);

        System.out.println("The min value is " + minMaxSort[0]);
        System.out.println("The max value is " + minMaxSort[1]);
    }

    public static int[] findMinMaxSort(int[] arr) { // O(n) space, O(n log n) time
        Arrays.sort(arr);

        int min = arr[0];
        int max = arr[arr.length - 1];

        return new int[]{min, max};
    }

    public static int[] findMinMax(int[] arr) { // O(n) time, O(1) space
        int min, max;
        min = max = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) min = arr[i];
            if (arr[i] > max) max = arr[i];
        }

        return new int[]{min, max};
    }
}
