package lesson1;

public class HW_ReverseString {

    public static void main(String[] args) {
        String target = "Coding challenges are fun and easy!";

        String reversed = reverse(target);
        System.out.println(reversed);
    }

    private static String reverse(String target) {
        String result = "";

        for (int i = target.length() - 1; i >= 0; i--)
            result += target.charAt(i);

        return result;
    }
}





/* HINTS
1. String data type has built-in method charAt() to access individual characters
2. You can traverse the dataset backwards using reverse "for" loop
3. Make use of += operator to add characters to string
    "a" += "b" results in "ab"
 */
