package lesson3;

import java.util.ArrayList;
import java.util.LinkedList;

public class OurLists {

    public static void main(String[] args) {
        String[] numbers = new String[4];
        numbers[0] = "one";
        numbers[2] = "three";
        numbers[3] = "four";

        ArrayList<String> numbersList = new ArrayList<>();
        numbersList.add("one");
        numbersList.add("two");
        numbersList.add("three");


        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("elemment");

        Node head = new Node(6);
        head.next = new Node(10);
        head.next.next = new Node(-3);

        System.out.println(head.value);
        System.out.println(head.next.value);
        System.out.println(head.next.next.value);

    }


}

class Node {
    int value;
    Node next;

    Node (int value) {
        this.value = value;
        next = null;
    }
}
