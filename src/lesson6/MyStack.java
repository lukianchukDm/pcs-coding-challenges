package lesson6;

import java.util.NoSuchElementException;

public class MyStack {
    private static class Node {
        private int data;
        private Node next;

        private Node(int data) {
            this.data = data;
        }

        private Node top;

        public boolean isEmpty() {
            return top == null;
        }

        public int peek() {
            if (isEmpty()) throw new NoSuchElementException("The stack is empty");

            return top.data;
        }

        public void push(int data) {
            Node node = new Node(data);

            node.next = top;

            top = node;
        }

        public int pop() {
            if (isEmpty())
                throw new NoSuchElementException("The stack is empty");

            int data = top.data;

            top = top.next;

            return data;
        }
    }
}
