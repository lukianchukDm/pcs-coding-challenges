package lesson6;

import java.util.NoSuchElementException;

public class MyQueue {
    private static class Node {
        private int data;
        private Node next;

        private Node(int data) {
            this.data = data;
        }

    }

    private Node head; // remove data from the head
    private Node tail; // add data

    public boolean isEmpty() {
        return head == null;
    }

    public int peek() {
        if (isEmpty()) throw new NoSuchElementException("The queue is empty");

        return head.data;
    }

    public void add(int data) {
        Node node = new Node(data);

        if (tail != null)
            tail.next = node;
        tail = node;

        if (isEmpty()) head = node;
    }

    public int remove() {
        int data = head.data;

        head = head.next;

        if (isEmpty())
            tail = null;

        return data;
    }
}
