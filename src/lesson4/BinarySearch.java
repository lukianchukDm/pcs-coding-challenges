package lesson4;

public class BinarySearch {

    public static void main(String[] args) {
        boolean res = binarySearch(new int[]{1, 3, 5, 16, 34, 54, 97}, 5);
        System.out.println(res);
        boolean res1 = binarySearchIter(new int[]{1, 3, 5, 16, 34, 54, 97}, 5);
        System.out.println(res1);
    }

    private static boolean binarySearch(int[] arr, int value) {
        return binarySearch(arr, value, 0, arr.length - 1);
    }

    private static boolean binarySearch(int[] arr, int value, int left, int right) {
        // implementation
        if (left > right) return false;

//        int mid = (left + right) / 2;
        int mid = left + ((right - left)) / 2;

        if (arr[mid] == value) return true;
        else if (value < arr[mid])
            return binarySearch(arr, value, left, mid - 1);
        else
            return binarySearch(arr, value, mid + 1, right);
    }

    private static boolean binarySearchIter(int[] arr, int value) {
        int left = 0, right = arr.length - 1;

        while (left <= right) {
            int mid = left + ((right - left)) / 2;

            if (arr[mid] == value) return true;
            else if (value < arr[mid]) right = mid - 1;
            else left = mid + 1;
        }

        return false;
    }
}
