package lesson4;

public class Recursion {

    public static void main(String[] args) {
//        System.out.println(fib(6));
//        System.out.println(fibMemo(6));
        System.out.println(factorialIter(4));
//        System.out.println(factorialMemo(5));
    }

    public static int fib(int n) {
//        if (n == 0) return 0;
//        else if (n == 1) return 1;

        if (n <= 1) return n;

        else return fib(n - 1) + fib(n - 2);
    }

    public static int fibMemo(int n) {
        int[] memo = new int[n + 1];

        if (n <= 1) return n;
        else if (memo[n] == 0) {
            memo[n] = fib(n - 1) + fib(n - 2);
        }
        return memo[n];
    }

    //TODO: implement iterative solution for fibo
    public static int fibIter(int n) {
        int prev = 0;
        int current = 1;
        int sum = 0;

        for (int i = 1; i < n; i++) {
            sum = prev + current;
            prev = current;
            current = sum;
        }

         return sum;
    }

    public static int factorial(int n) {
        if (n <= 1) return 1;
        else return n * factorial(n - 1);
    }

    public static int factorialMemo(int n) {
        if (n <= 1) return 1;

        int[] memo = new int[n + 1];
        if (memo[n] == 0)
            memo[n] = n * factorial(n - 1);

        return memo[n];
    }

    //TODO: implement iterative solution for factorial
    public static int factorialIter(int n) {
        int result = 1, i = 1;

        while (i <= n) {
            result = result * i;
            i++;
        }

        return result;
    }
}
