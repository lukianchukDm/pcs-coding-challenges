package lesson4;

public class HW_BinarySearch {

    public static void main(String[] args) {
        boolean res = binarySearch(new String[]{"apple", "banana", "mango", "orange", "pineapple"}, "banana");
        System.out.println(res);

//        boolean res1 = binarySearch(new String[]{"apple", "banana", "mango", "orange", "pineapple"}, "mushroom");
//        System.out.println(res1);
    }

    private static boolean binarySearch(String[] arr, String value) {
        int left = 0, right = arr.length - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            System.out.println(arr[mid].compareTo(value));
            "apple".equals("apple");
            "example".contains("a");


            if (arr[mid].compareTo(value) == 0) return true;
            if (arr[mid].compareTo(value) > 0) right = mid - 1;
            else left = mid + 1;
        }

        return false;
    }
}


/* HINTS
1. To compare Strings use a function compareTo()
    it returns 0 for equal Strings
    negative value if the String is lexicographically is less that target
    positive value if the String is lexicographically is greater that target
    (includes the alphabetical order)
 */
