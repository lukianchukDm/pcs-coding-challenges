package lesson2;

import java.util.HashSet;

public class FirstDuplicate {

    public static void main(String[] args) {
        int[] arr = {115, 24, 5, 33, 33, 115, 7};

//        System.out.println(firstDuplicate(arr));
        System.out.println(firstDuplicateHashSet(arr));

        HashSet<String> hashSet = new HashSet<>();

        hashSet.add("one");
        hashSet.add("two");
        hashSet.add("three");

        hashSet.remove("three");

        System.out.println(hashSet);
        System.out.println(hashSet.contains("one"));
        System.out.println(hashSet.contains("four"));

        hashSet.isEmpty();
        hashSet.clear();
    }

    private static int firstDuplicateHashSet(int[] arr) {
        HashSet<Integer> seen = new HashSet<>();

        for (int element : arr) { // O(n) time, space
            if (seen.contains(element)) return element;
            else seen.add(element);
        }

        return -1;
    }

    private static int firstDuplicate(int[] arr) {
        int duplicateIndex = arr.length;

        for (int i = 0; i < arr.length; i++) { // time O(n^2)
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) duplicateIndex = Math.min(duplicateIndex, j);
            }
        }

        if (duplicateIndex == arr.length) return -1;
        else return arr[duplicateIndex];
    }

}
