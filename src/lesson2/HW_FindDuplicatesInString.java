package lesson2;

public class HW_FindDuplicatesInString {

    public static void main(String[] args) {
        String sentence = "Coding challenges become more and more interesting!";

        System.out.println(findDuplicates(sentence));
    }

    private static String findDuplicates(String sentence) {
        String seenChars = "";
        String duplicates = "";

        for (int i = 0; i < sentence.length(); i++) {
            String currentChar = Character.toString(sentence.charAt(i));
            if (seenChars.contains(currentChar) && !duplicates.contains(currentChar))
                duplicates += currentChar;

            seenChars += currentChar;
        }

        if (duplicates.isEmpty()) return "_";
        return duplicates;
    }
}

/* HINTS
1. Store the characters that you have already seen and check all the new characters against this list to find duplicates
2. String can be used as a storage
3. Don't forget to return something in case there are no duplicates
 */