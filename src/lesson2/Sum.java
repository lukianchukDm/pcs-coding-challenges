package lesson2;

import java.util.Arrays;
import java.util.HashMap;

public class Sum {

    public static void main(String[] args) {
        int[] arr = { 2, 7, 11, 15 };
        int target = 9;

//        int[] res = targetSum(arr, target);
        int[] res = targetSumHashMap(arr, target);

        System.out.println(Arrays.toString(res));

        HashMap<String, Integer> hashMap = new HashMap<>();

        hashMap.put("a", 10);
        hashMap.put("b", 15);
        hashMap.put("c", -7);

        System.out.println(hashMap.get("a"));

        hashMap.remove("b");

        System.out.println(hashMap.containsKey("c"));
        System.out.println(hashMap.containsValue(100));

        hashMap.replace("a", 111);
        System.out.println(hashMap);


    }

    private static int[] targetSumHashMap(int[] arr, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i <arr.length; i++) { //O(n) time, space
            int compliment = target - arr[i];

            if (map.containsKey(compliment)) return new int[]{ map.get(compliment), i };
            map.put(arr[i], i);

        }

        throw new IllegalArgumentException("Target can not be met");
    }

    private static int[] targetSum(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) { // O(n^2) time
            for (int j = i + 1; j < arr.length; j++) {
                int compliment = target - arr[i];

                if (arr[j] == compliment) return new int[] {i, j};
            }
        }

        throw new IllegalArgumentException("Target can not be met");
    }
}
