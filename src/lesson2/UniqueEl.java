package lesson2;

import java.util.HashMap;

public class UniqueEl {

    public static void main(String[] args) {
        String input = "abcabc";

        System.out.println(findFirstNonRepeatingChar(input));
    }

    private static char findFirstNonRepeatingChar(String input) { // time O(n)
        HashMap<Character, Integer> count = new HashMap<>();

        for (int i = 0; i < input.length(); i++) {
            char character = input.charAt(i);

            if (count.containsKey(character))
                count.put(character, count.get(character) + 1);
            else
                count.put(character, 1);
        }

        for (int i = 0; i < input.length(); i++) {
            char character = input.charAt(i);

            if (count.get(character) == 1) return character;

        }

        return '_';
    }
}
