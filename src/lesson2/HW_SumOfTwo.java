package lesson2;

import java.util.HashSet;

public class HW_SumOfTwo {

    public static void main(String[] args) {
        int[] arr1 = { 10, 20, 30 };
        int[] arr2 = { 1, 2, 3, 4, 5 };

        int target1 = 25;

        int[] arr3 = { 0, -1, -2 };
        int[] arr4 = { 5, 5, 5 };

        int target2 = 100;

        System.out.println(sumOfTwo(arr1, arr2, target1));
        System.out.println(sumOfTwo(arr3, arr4, target2));
    }

    private static boolean sumOfTwo(int[] arr1, int[] arr2, int target) {
        HashSet<Integer> compliments = new HashSet<>();

        for (int value1 : arr1) {
            int compliment = target - value1;
            compliments.add(compliment);
        }

        for(int value2 : arr2) {
            if (compliments.contains(value2)) return true;
        }

        return false;
    }
}


/* HINTS
1. Use external memory to minimize time complexity
2. Find the different between value in first array and the target value - this is the value you're gonna be looking for in the second array
 */